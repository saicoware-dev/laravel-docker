FROM php:7.4-fpm-alpine

RUN apk add --update libzip-dev zip freetype-dev libpng-dev libjpeg-turbo-dev libxml2-dev autoconf g++ imagemagick-dev libtool npm make \
    && docker-php-ext-configure gd \
        --with-freetype=/usr/include/ \
        --with-jpeg=/usr/include/ \
    && docker-php-ext-install gd \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install pdo \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && docker-php-ext-install soap \
    && docker-php-ext-install zip \
    && docker-php-ext-install exif \
    && pecl install imagick \
    && docker-php-ext-enable imagick exif \
    && apk del autoconf g++ libtool make \
    && rm -rf /tmp/* /var/cache/apk/* \
    && chown -R www-data:www-data /var/www/html/
COPY --from=composer /usr/bin/composer /usr/bin/composer
