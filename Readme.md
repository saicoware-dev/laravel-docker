# How to Install

You need docker installed and running, with docker tools or docker desktop

## Quick Setup for MAC or Linux users

* git clone git@bitbucket.org:saicoware-dev/laravel-docker.git
* cd laravel-docker
* sudo ./run

## Manual Setup

* git clone git@bitbucket.org:saicoware-dev/laravel-docker.git
* cd laravel-docker

## Build and Run the docker containers

* docker-compose build && docker-compose up -d

## Checkout your working branch

* cd src
* git checkout -b <the-branch-that-you-are-going-to-work-on>

## Install dependencies and Finish the setup

* docker-compose run --rm composer install
* cp ./src/.env.development ./src/.env
* docker-compose run --rm artisan key:generate
* docker-compose run --rm artisan config:cache

## That's it. We are Done.

* Go to localhost:8080/ on your browser and start committing your work

## Need to run composer or artisan?

### Composer

Needs to be executed inside the docker instance, like this:

* docker-compose run --rm composer require package-user-name/package-name

or:

* docker-compose run --rm composer update

### Artisan

Also needs to be executed inside the docker instance, like this:

* docker-compose run --rm artisan config:cache

or:

* docker-compose run --rm artisan key:generate

(You get the idea)

## Ready to work? Or, are you going to work on new Module?

* Please read the Readme.md file inside ./src. You will find important information about how we like to work with modules

## Finished your tasks?

* Please, make sure you are committing your (well documented) changes to your branch only
* Create a pull request
